package services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import server.StoreID;
import server.DAO.RoomDAO;
import server.DAO.UserDAO;
import server.DTO.*;
import server.entity.Room;
import server.entity.User;
import server.exceptions.UserException;
import server.service.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserDAO userDao;

    @Mock
    private RoomDAO roomDao;

    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void setup() {
        StoreID.setId(1);
        when(userDao.getUserByUsernameAndPassword("rinor", "rinori123")).thenReturn(new User(1, "rinor", "rinori123", null, null));

        List<User> sampleUsers = new ArrayList<>();
        User user = new User(1, "rinor", "rinori123", null, null);
        sampleUsers.add(user);
        when(userDao.findAll()).thenReturn(sampleUsers);

        when(userDao.findById(1)).thenReturn(java.util.Optional.of(user));

        Room room = new Room("football");
        room.setId(1);
        when(roomDao.findById(1)).thenReturn(java.util.Optional.of(room));
        when(userDao.save(any())).thenReturn(user);
    }

    @Test
    public void getUsers_withNoArg_returnListOfUserTransport() {
        List<UserTransport> users = userService.getUsers();
        assertThat(users.get(0).getId()).isEqualTo(1);
        assertThat(users.get(0).getUsername()).isEqualTo("rinor");
        assertEquals(users.size(), 1);
    }

    @Test(expected = UserException.class)
    public void getUsers_withNoArg_returnUserException() {
        when(userDao.findAll()).thenReturn(null);
        List<UserTransport> users = userService.getUsers();
        assertThat(users.get(0).getId()).isEqualTo(1);
        assertThat(users.get(0).getUsername()).isEqualTo("rinor");
        assertEquals(users.size(), 1);
    }

    @Test
    public void getUserToken_byUsernameAndPassword_returnCorrespondingToken() {
        UserTokenTransport userTokenTransport = userService.getUserTokenByUsernameAndPassword("rinor", "rinori123");
        assertThat(userTokenTransport.getToken()).isEqualTo("1");
    }

    @Test(expected = UserException.class)
    public void getUserToken_byUsernameAndPassword_returnUserException() {
        when(userDao.getUserByUsernameAndPassword("rinor", "rinori123")).thenReturn(null);
        UserTokenTransport userTokenTransport = userService.getUserTokenByUsernameAndPassword("rinor", "rinori123");
        assertThat(userTokenTransport.getToken()).isEqualTo("1");
    }

    @Test
    public void createUser_byUsernameAndPassword_returnInfoTransport() {
        InfoTransport infoTransport = userService.createUser("rinor", "rinori123");
        assertEquals(infoTransport.getMessage(), "Registered successfully");
    }

    @Test(expected = UserException.class)
    public void createUser_byUsernameAndPassword_returnUserException() {
        when(userDao.save(any())).thenReturn(null);
        InfoTransport infoTransport = userService.createUser("rinor", "rinori123");
        assertEquals(infoTransport.getMessage(), "Registered successfully");
    }

    @Test
    public void updateUserCredentials_withUsernameAndPassword_returnInfoTransport() {
        InfoTransport infoTransport = userService.updateUserCredentials("rinor", "rinori123");
        assertEquals(infoTransport.getMessage(),"User updated successfully");
    }

    @Test(expected = UserException.class)
    public void updateUserCredentials_withUsernameAndPassword_returnUserException() {
        when(userDao.save(any())).thenReturn(null);
        InfoTransport infoTransport = userService.updateUserCredentials("rinor", "rinori123");
        assertEquals(infoTransport.getMessage(),"User updated successfully");
    }

    @Test
    public void updateUserRoomId_withRoomId_returnUserRoomUpdationTransport() {
        UserRoomUpdationTransport userRoomUpdationTransport = userService.updateUserRoomId("1");
        assertEquals(userRoomUpdationTransport.getRoomId(),"1");
    }

    @Test(expected = UserException.class)
    public void updateUserRoomId_withRoomId_returnUserException() {
        when(userDao.save(any())).thenReturn(null);
        UserRoomUpdationTransport userRoomUpdationTransport = userService.updateUserRoomId("1");
        assertEquals(userRoomUpdationTransport.getRoomId(),"1");
    }

    @Test
    public void deleteUser_byId_returnUserTransport() {
        int userId = 1;
        InfoTransport infoTransport = userService.deleteUser(userId);
        assertEquals(infoTransport.getMessage(), "Deleted user " + userId);
    }

}