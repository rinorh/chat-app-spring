package services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import server.StoreID;
import server.DAO.MessageDAO;
import server.DAO.UserDAO;
import server.DTO.InfoTransport;
import server.DTO.MessageTransport;
import server.entity.Message;
import server.entity.Room;
import server.entity.User;
import server.exceptions.MessageException;
import server.service.MessageServiceImpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {

    @Mock
    private MessageDAO messageDao;

    @Mock
    private UserDAO userDao;

    @InjectMocks
    private MessageServiceImpl messageService;

    @Before
    public void setup() {
        StoreID.setId(1);

        Room room = new Room("football");
        room.setId(1);

        User user = new User(1, "rinor", "rinori123", null, null);

        Message message1 = new Message();
        message1.setId(1);
        message1.setContent("hello");
        message1.setUserId(user);
        message1.setTimestamp(new Timestamp(2));

        Message message2 = new Message();
        message2.setId(2);
        message2.setContent("hello 2");
        message2.setUserId(user);
        message1.setTimestamp(new Timestamp(4));

        List<Message> messages = new ArrayList<>();
        messages.add(message1);
        messages.add(message2);

        room.setMessages(messages);
        message1.setRoomId(room);
        message2.setRoomId(room);
        user.setRoomId(room);
        user.setMessages(messages);

        when(messageDao.findAll()).thenReturn(messages);
        when(messageDao.findById(1)).thenReturn(java.util.Optional.of(message1));
        when(userDao.findById(1)).thenReturn(java.util.Optional.of(user));
        when(messageDao.save(any())).thenReturn(message1);
    }

    @Test
    public void save_message_returnInfoTransport() {
        InfoTransport infoTransport = messageService.saveMessage("hej");
        assertEquals(infoTransport.getMessage(), "Message sent successfully");
    }

    @Test(expected = MessageException.class)
    public void save_message_returnMessageException() {
        when(messageDao.save(any())).thenReturn(null);
        InfoTransport infoTransport = messageService.saveMessage("hej");
        assertEquals(infoTransport.getMessage(), "Message sent successfully");
    }

    @Test
    public void updateMessage_byMessageIdAndContent_returnInfoTransport() {
        InfoTransport infoTransport = messageService.updateMessage(1, "hej2");
        assertEquals(infoTransport.getMessage(), "Message was updated successfully");
    }

    @Test(expected = MessageException.class)
    public void updateMessage_byMessageIdAndContent_returnMessageException() {
        when(messageDao.save(any())).thenReturn(null);
        InfoTransport infoTransport = messageService.updateMessage(1, "hej2");
        assertEquals(infoTransport.getMessage(), "Message was updated successfully");
    }

}
