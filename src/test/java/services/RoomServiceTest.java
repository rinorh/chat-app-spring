package services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import server.DAO.RoomDAO;
import server.DTO.InfoTransport;
import server.DTO.RoomIdTransport;
import server.DTO.RoomTransport;
import server.entity.Room;
import server.exceptions.RoomException;
import server.service.RoomServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RoomServiceTest {

    @Mock
    private RoomDAO roomDao;

    @InjectMocks
    private RoomServiceImpl roomService;

    @Before
    public void setup() {
        Room room = new Room();
        room.setId(1);
        room.setName("football");
        List<Room> rooms = new ArrayList<>();
        rooms.add(room);
        when(roomDao.findAll()).thenReturn(rooms);
        when(roomDao.findById(1)).thenReturn(java.util.Optional.of(room));
        when(roomDao.save(any())).thenReturn(room);
    }

    @Test
    public void getRooms_withNoArg_returnListOfRoomTransport() {
        List<RoomTransport> rooms = roomService.getRooms();
        assertEquals(rooms.get(0).getId(), 1);
        assertEquals(rooms.get(0).getName(), "football");
        assertEquals(rooms.size(), 1);
    }

    @Test(expected = RoomException.class)
    public void getRooms_withNoArg_returnRoomException() {
        when(roomDao.findAll()).thenReturn(null);
        List<RoomTransport> rooms = roomService.getRooms();
        assertEquals(rooms.get(0).getId(), 1);
        assertEquals(rooms.get(0).getName(), "football");
        assertEquals(rooms.size(), 1);
    }

    @Test
    public void getRoom_byId_returnRoomTransport() {
        RoomTransport roomTransport = roomService.getRoom(1);
        assertEquals(roomTransport.getId(), 1);
        assertEquals(roomTransport.getName(), "football");
    }

    @Test(expected = RoomException.class)
    public void getRoom_byId_returnRoomException() {
        when(roomDao.findById(1)).thenReturn(Optional.empty());
        RoomTransport roomTransport = roomService.getRoom(1);
        assertEquals(roomTransport.getId(), 1);
        assertEquals(roomTransport.getName(), "football");
    }

    @Test
    public void createRoom_byRoomName_returnRoomIdTransport() {
        RoomIdTransport roomIdTransport = roomService.createRoom("football");
        assertEquals(roomIdTransport.getRoomId(), 1);
    }

    @Test
    public void updateRoom_byRoomNameAndRoomId_returnInfoTransport() {
        InfoTransport infoTransport = roomService.updateRoom(1, "football");
        assertEquals(infoTransport.getMessage(), "Room updated successfully!");
    }

    @Test(expected = RoomException.class)
    public void updateRoom_byRoomNameAndRoomId_returnRoomException() {
        when(roomDao.save(any())).thenReturn(null);
        InfoTransport infoTransport = roomService.updateRoom(1, "football");
        assertEquals(infoTransport.getMessage(), "Room updated successfully!");
    }

    @Test
    public void deleteRoom_byId_returnInfoTransport() {
        int roomId = 1;
        InfoTransport infoTransport = roomService.deleteRoom(1);
        assertEquals(infoTransport.getMessage(), "Deleted room " + roomId);
    }

}
