package server.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import server.DTO.*;
import server.service.UserService;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public UserListTransport getUsers() {
        return new UserListTransport(userService.getUsers());
    }

    @GetMapping("/{userId}")
    public UserTransport getUser(@PathVariable int userId) {
        return userService.getUser(userId);
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public InfoTransport createUser(@RequestBody UserCredentialsTransport userCredentialsTransport) {
        return userService.createUser(userCredentialsTransport.getUsername(), userCredentialsTransport.getPassword());
    }

    @PutMapping("/{userId}/credentials")
    public InfoTransport updateUserCredentials(@RequestBody UserCredentialsTransport userCredentialsTransport) {
        return userService.updateUserCredentials(userCredentialsTransport.getUsername(), userCredentialsTransport.getPassword());
    }

    @PutMapping("/{userId}")
    public UserRoomUpdationTransport updateUserRoom(@RequestBody UserRoomUpdationTransport userRoomUpdationTransport) {
        return userService.updateUserRoomId(userRoomUpdationTransport.getRoomId());
    }

    @DeleteMapping("/{userId}")
    public InfoTransport deleteUser(@PathVariable int userId) {
        return userService.deleteUser(userId);
    }

}
