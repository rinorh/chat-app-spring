package server.rest;

import org.springframework.web.bind.annotation.*;
import server.DTO.*;
import server.service.RoomService;

import java.util.List;

@RestController
@RequestMapping("/v1/rooms")
public class RoomController {

    private RoomService roomService;

    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @GetMapping("/")
    public RoomListTransport getRooms() {
        return new RoomListTransport(roomService.getRooms());
    }

    @GetMapping("/{roomId}")
    public RoomTransport getRoom(@PathVariable int roomId) {
        return roomService.getRoom(roomId);
    }

    @PostMapping("/")
    public RoomIdTransport createRoom(@RequestBody RoomNameTransport roomNameTransport) {
        return roomService.createRoom(roomNameTransport.getRoomName());
    }

    @PutMapping("/{roomId}")
    public InfoTransport updateRoom(@PathVariable int roomId, @RequestBody RoomNameTransport roomNameTransport) {
        return roomService.updateRoom(roomId, roomNameTransport.getRoomName());
    }

    @DeleteMapping("/{roomId}")
    public InfoTransport deleteRoom(@PathVariable int roomId) {
        return roomService.deleteRoom(roomId);
    }

}
