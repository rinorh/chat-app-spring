package server.rest;

import org.springframework.web.bind.annotation.*;
import server.DTO.MessageContentTransport;
import server.DTO.MessageListTransport;
import server.StoreID;
import server.DTO.InfoTransport;
import server.DTO.MessageTransport;
import server.service.MessageService;

@RestController
@RequestMapping("/v1/messages")
public class MessageController {

    private MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public MessageListTransport getUnreadMessages(@RequestParam long lastTimeRead) {
        return new MessageListTransport(messageService.getUnreadMessages(StoreID.getId(), lastTimeRead));
    }

    @PostMapping("/")
    public InfoTransport sendMessage(@RequestBody MessageContentTransport messageContentTransport) {
        return messageService.saveMessage(messageContentTransport.getContent());
    }

    @PutMapping("/{messageId}")
    public InfoTransport updateMessage(@PathVariable int messageId, @RequestBody MessageContentTransport messageContentTransport) {
        return messageService.updateMessage(messageId, messageContentTransport.getContent());
    }

    @DeleteMapping("/{messageId}")
    public InfoTransport deleteMessage(@PathVariable int messageId) {
        return messageService.deleteMessage(messageId);
    }
}
