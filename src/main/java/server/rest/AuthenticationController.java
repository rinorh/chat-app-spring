package server.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import server.DTO.UserCredentialsTransport;
import server.DTO.UserTokenTransport;
import server.service.UserService;

@RestController
@RequestMapping("/v1/auth")
public class AuthenticationController {

    private UserService userService;

    public AuthenticationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/")
    public UserTokenTransport login(@RequestBody UserCredentialsTransport userCredentialsTransport) {
        return userService.getUserTokenByUsernameAndPassword(userCredentialsTransport.getUsername(), userCredentialsTransport.getPassword());
    }

}
