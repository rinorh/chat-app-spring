package server.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import server.DAO.MessageDAO;
import server.DAO.RoomDAO;
import server.DAO.RoomVisitDAO;
import server.entity.Message;
import server.entity.Room;
import server.entity.RoomVisit;
import server.entity.User;


@Component
public class MessageTask {

    private RoomVisitDAO roomVisitDAO;
    private RoomDAO roomDAO;
    private MessageDAO messageDAO;

    public MessageTask(RoomVisitDAO roomVisitDAO, RoomDAO roomDAO, MessageDAO messageDAO) {
        this.roomVisitDAO = roomVisitDAO;
        this.roomDAO = roomDAO;
        this.messageDAO = messageDAO;
    }

    @Scheduled(fixedRateString = "${MONTH}")
    public void deleteOldMessages() {
        findOldMessagesAndDeleteThem();
    }

    private void findOldMessagesAndDeleteThem() {
        for(Room room : roomDAO.findAll()) {
            findOldMessagesInRoomAndDeleteThem(room);
        }
    }

    private void findOldMessagesInRoomAndDeleteThem(Room room) {
        for(Message message : room.getMessages()) {
            if(isMessageOld(message, room)) {
                messageDAO.delete(message);
            }
        }
    }

    private boolean isMessageOld(Message message, Room room) {
        for(RoomVisit roomVisit : room.getRoomVisits()) {
            if(!hasBeenSeenByThisUser(roomVisit.getUserId(), message)) return false;
        }
        return true;
    }

    private boolean hasBeenSeenByThisUser(User user, Message message) {
        return user.getRoomId().getId() == message.getRoomId().getId() || roomVisitDAO.getRoomVisitByUserIdAndRoomId(user.getId(), message.getRoomId().getId()).getLastTimeInRoom().compareTo(message.getTimestamp()) > 0;
    }

}
