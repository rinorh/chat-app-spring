package server.service;

import server.DTO.InfoTransport;
import server.DTO.RoomIdTransport;
import server.DTO.RoomTransport;

import java.util.List;

public interface RoomService {

    List<RoomTransport> getRooms();

    RoomTransport getRoom(int roomId);

    RoomIdTransport createRoom(String roomName);

    InfoTransport updateRoom(int roomId, String roomName);

    InfoTransport deleteRoom(int roomId);

}
