package server.service;

import server.DTO.InfoTransport;
import server.DTO.MessageTransport;

import java.util.List;

public interface MessageService {

    List<MessageTransport> getUnreadMessages(int userId, long lastTimeRead);

    InfoTransport saveMessage(String content);

    InfoTransport updateMessage(int messageId, String content);

    InfoTransport deleteMessage(int messageId);

}
