package server.service;

import org.springframework.stereotype.Service;
import server.DAO.RoomVisitDAO;
import server.StoreID;
import server.DAO.RoomDAO;
import server.DAO.UserDAO;
import server.DTO.*;
import server.entity.Room;
import server.entity.RoomVisit;
import server.entity.User;
import server.exceptions.RoomException;
import server.exceptions.UserException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserDAO userDao;
    private RoomDAO roomDao;

    public UserServiceImpl(UserDAO userDao, RoomDAO roomDao) {
        this.userDao = userDao;
        this.roomDao = roomDao;
    }

    @Override
    public List<UserTransport> getUsers() {
        List<User> users = userDao.findAll();
        if(users == null) throw new UserException("No users found!");
        List<UserTransport> userTransports = new ArrayList<>();
        for(User user : users) {
            userTransports.add(new UserTransport(user.getId(), user.getUsername()));
        }
        return userTransports;
    }

    @Override
    public UserTokenTransport getUserTokenByUsernameAndPassword(String username, String password) {
        User user = userDao.getUserByUsernameAndPassword(username, password);
        if(user == null) throw new UserException("LOGIN_ERROR");
        return new UserTokenTransport(Integer.toString(user.getId()));
    }

    @Override
    public UserTransport getUser(int userId) {
        User user = userDao.findById(userId).orElseThrow(() -> new UserException("User not found!"));
        return new UserTransport(user.getId(), user.getUsername());
    }

    @Override
    public User getUserByToken(String token) {
        Optional<User> userOptional =  userDao.findById(Integer.parseInt(token));
        User user = userOptional.orElse(null);
        if(user == null) throw new UserException("User not found!");
        return user;
    }

    @Override
    public InfoTransport createUser(String username, String password) {
        User savedUser = userDao.save(new User(username, password));
        if(savedUser != null) return new InfoTransport("Registered successfully");
        else throw new UserException("User not saved!");
    }

    @Override
    public InfoTransport deleteUser(int userId) {
        Optional<User> userOptional = userDao.findById(StoreID.getId());
        User user = userOptional.orElse(null);
        if(user == null) throw new UserException("User not found!");
        userDao.deleteById(userId);
        return new InfoTransport("Deleted user " + userId);
    }

    @Override
    public InfoTransport updateUserCredentials(String username, String password) {
        Optional<User> userOptional = userDao.findById(StoreID.getId());
        User user = userOptional.orElse(null);
        if(user == null) throw new UserException("User not found!");
        user.setUsername(username);
        user.setPassword(password);
        User updatedUser = userDao.save(user);
        if(updatedUser != null) return new InfoTransport("User updated successfully");
        else throw new UserException("User was not updated!");
    }

    @Override
    public UserRoomUpdationTransport updateUserRoomId(String roomId) {
        Room room = null;
        if(roomId != null) {
            Optional<Room> roomOptional = roomDao.findById(Integer.parseInt(roomId));
            room = roomOptional.orElse(null);
            if(room == null) throw new RoomException("ROOM_NOT_FOUND");
        } else {
            roomVisitDAO.save(new RoomVisit());
        }
        Optional<User> userOptional = userDao.findById(StoreID.getId());
        User user = userOptional.orElse(null);
        if(user == null) throw new UserException("User not found!");
        user.setRoomId(room);
        User saved = userDao.save(user);
        if(saved != null) return new UserRoomUpdationTransport(roomId);
        else throw new UserException("User was not saved!");
    }

}
