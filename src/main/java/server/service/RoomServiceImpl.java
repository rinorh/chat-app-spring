package server.service;

import org.springframework.stereotype.Service;
import server.DAO.RoomDAO;
import server.DTO.InfoTransport;
import server.DTO.RoomIdTransport;
import server.DTO.RoomTransport;
import server.entity.Room;
import server.exceptions.RoomException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoomServiceImpl implements RoomService {

    private RoomDAO roomDao;

    public RoomServiceImpl(RoomDAO roomDao) {
        this.roomDao = roomDao;
    }

    @Override
    public List<RoomTransport> getRooms() {
        List<Room> rooms = roomDao.findAll();
        if(rooms == null) throw new RoomException("Rooms could not be fetched!");
        List<RoomTransport> roomsReceived = new ArrayList<>();
        for(Room room : rooms) {
            roomsReceived.add(new RoomTransport(room.getId(), room.getName()));
        }
        return roomsReceived;
    }

    @Override
    public RoomTransport getRoom(int roomId) {
        Optional<Room> roomOptional = roomDao.findById(roomId);
        Room room = roomOptional.orElse(null);
        if(room == null) throw new RoomException("ROOM_NOT_FOUND");
        return new RoomTransport(room.getId(), room.getName());
    }

    @Override
    public RoomIdTransport createRoom(String roomName) {
        Room roomSaved = roomDao.save(new Room(roomName));
        if(roomSaved == null) throw new RoomException("Room could not be saved!");
        return new RoomIdTransport(roomSaved.getId());
    }

    @Override
    public InfoTransport updateRoom(int roomId, String roomName) {
        Optional<Room> roomOptional = roomDao.findById(roomId);
        Room room = roomOptional.orElse(null);
        if(room == null) throw new RoomException("Room could not be found!");
        room.setName(roomName);
        Room roomUpdated = roomDao.save(room);
        if(roomUpdated == null) throw new RoomException("Room could not be updated!");
        return new InfoTransport("Room updated successfully!");
    }


    @Override
    public InfoTransport deleteRoom(int roomId) {
        roomDao.deleteById(roomId);
        return new InfoTransport("Deleted room " + roomId);
    }

}
