package server.service;

import org.springframework.stereotype.Service;
import server.DAO.RoomVisitDAO;
import server.DTO.UserListTransport;
import server.DTO.UserTransport;
import server.StoreID;
import server.DAO.MessageDAO;
import server.DAO.UserDAO;
import server.DTO.InfoTransport;
import server.DTO.MessageTransport;
import server.entity.Message;
import server.entity.RoomVisit;
import server.entity.User;
import server.exceptions.MessageException;
import server.exceptions.UserException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {

    private MessageDAO messageDao;
    private UserDAO userDao;
    private RoomVisitDAO roomVisitDAO;

    public MessageServiceImpl(MessageDAO messageDao, UserDAO userDao, RoomVisitDAO roomVisitDAO) {
        this.messageDao = messageDao;
        this.userDao = userDao;
        this.roomVisitDAO = roomVisitDAO;
    }

    @Override
    public List<MessageTransport> getUnreadMessages(int userId, long lastTimeRead) {
        Optional<User> userOptional = userDao.findById(userId);
        User user = userOptional.orElse(null);
        if(user == null) throw new UserException("User not found!");
        Timestamp lastTime = new Timestamp(lastTimeRead);
        List<Message> messages = user.getRoomId().getMessages().stream().sorted().filter(message -> (message.getTimestamp().after(lastTime))).collect(Collectors.toList());
        List<MessageTransport> messagesTransport = new ArrayList<>();
        for(int i = messages.size() - 1; i >= 0; i--) {
            List<UserTransport> userTransports = getUsersWhoHaveSeenThisMessage(messages.get(0));
            messagesTransport.add(0, new MessageTransport(messages.get(i).getUserId().getUsername(), messages.get(i).getContent(), userTransports));
        }
        return messagesTransport;
    }

    private List<UserTransport> getUsersWhoHaveSeenThisMessage(Message message) {
        List<UserTransport> userListTransport = new ArrayList<>();
        for(RoomVisit roomVisit : message.getRoomId().getRoomVisits()) {
            User user = roomVisit.getUserId();
            if(hasBeenSeenByThisUser(user, message)) userListTransport.add(new UserTransport(user.getId(), user.getUsername()));
        }
        return userListTransport;
    }

    private boolean hasBeenSeenByThisUser(User user, Message message) {
        return user.getRoomId().getId() == message.getRoomId().getId() || roomVisitDAO.getRoomVisitByUserIdAndRoomId(user.getId(), message.getRoomId().getId()).getLastTimeInRoom().after(message.getTimestamp());
    }

    @Override
    public InfoTransport saveMessage(String content) {
        Optional<User> userOptional = userDao.findById(StoreID.getId());
        User user = userOptional.orElse(null);
        if(user == null) throw new UserException("User not found!");
        Message savedMessage = messageDao.save(new Message(content, user, user.getRoomId()));
        if(savedMessage == null) throw new MessageException("Message could not be saved!");
        return new InfoTransport("Message sent successfully");
    }

    @Override
    public InfoTransport updateMessage(int messageId, String content) {
        Optional<Message> messageOptional =  messageDao.findById(messageId);
        Message message = messageOptional.orElse(null);
        if(message == null) throw new MessageException("Message not found!");
        if(StoreID.getId() != message.getUserId().getId()) throw new MessageException("Cannot update message!");
        message.setContent(content);
        Message savedMessage = messageDao.save(message);
        if(savedMessage == null) throw new MessageException("Message could not be updated!");
        return new InfoTransport("Message was updated successfully");
    }

    @Override
    public InfoTransport deleteMessage(int messageId) {
        Optional<Message> messageOptional =  messageDao.findById(messageId);
        Message message = messageOptional.orElse(null);
        if(message == null) throw new MessageException("Message not found!");
        messageDao.delete(message);
        return new InfoTransport("Message deleted successfully!");
    }

}
