package server.service;

import server.DTO.*;
import server.entity.User;

import java.util.List;

public interface UserService {

    List<UserTransport> getUsers();

    UserTransport getUser(int userId);

    UserTokenTransport getUserTokenByUsernameAndPassword(String username, String password);

    User getUserByToken(String token);

    InfoTransport createUser(String username, String password);

    InfoTransport deleteUser(int userId);

    InfoTransport updateUserCredentials(String username, String password);

    UserRoomUpdationTransport updateUserRoomId(String roomId);
}
