package server.exceptions;

public class RoomException extends RuntimeException {

    public RoomException(String message) {
        super(message);
    }

}
