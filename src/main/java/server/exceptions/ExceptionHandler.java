package server.exceptions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import server.DTO.ErrorResponse;

@ControllerAdvice
public class ExceptionHandler {

    private ObjectMapper objectMapper = new ObjectMapper();

    @org.springframework.web.bind.annotation.ExceptionHandler
    public ResponseEntity<String> handleException(UserException ex) {
        ErrorResponse errorResponse = null;
        HttpStatus httpStatus = null;
        switch (ex.getMessage()) {
            case "LOGIN_ERROR":
                httpStatus = HttpStatus.UNAUTHORIZED;
                errorResponse = new ErrorResponse(httpStatus.value(), ex.getMessage());
                break;
        }
        String jsonResponse = null;
        try {
            jsonResponse = objectMapper.writeValueAsString(errorResponse);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(jsonResponse, httpStatus);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler
    public ResponseEntity<String> handleException(RoomException ex) {
        ErrorResponse errorResponse = null;
        HttpStatus httpStatus = null;
        switch (ex.getMessage()) {
            case "ROOM_NOT_FOUND":
                httpStatus = HttpStatus.NOT_FOUND;
                errorResponse = new ErrorResponse(httpStatus.value(), ex.getMessage());
                break;
        }
        String jsonResponse = null;
        try {
            jsonResponse = objectMapper.writeValueAsString(errorResponse);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(jsonResponse, httpStatus);
    }

}
