package server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import server.service.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationContext extends OncePerRequestFilter {

    private UserService userService;

    public AuthenticationContext(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String uri = httpServletRequest.getRequestURI();
        if(uri.equals("/v1/auth/") || (uri.equals("/v1/users/") && httpServletRequest.getMethod().equals(HttpMethod.POST.name()))) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }
        String authorization = httpServletRequest.getHeader("authorization");
        String userId = authorization.substring(authorization.lastIndexOf(" ") + 1);
        if(userService.getUserByToken(userId) != null) {
            StoreID.setId(Integer.parseInt(userId));
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } else {
            httpServletResponse.sendError(403);
        }
    }
}
