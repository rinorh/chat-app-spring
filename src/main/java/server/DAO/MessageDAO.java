package server.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import server.entity.Message;

@Repository
public interface MessageDAO extends JpaRepository<Message, Integer> {

}
