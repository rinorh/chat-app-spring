package server.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import server.entity.RoomVisit;

@Repository
public interface RoomVisitDAO extends JpaRepository<RoomVisit, Integer> {

    @Query(value = "SELECT * FROM room_visit rv WHERE rv.user_id = ?1 AND rv.room_id = ?2", nativeQuery = true)
    RoomVisit getRoomVisitByUserIdAndRoomId(int user_id, int room_id);

}
