package server.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import server.entity.User;

@Repository
public interface UserDAO extends JpaRepository<User, Integer> {

    @Query(value = "SELECT * FROM user u WHERE u.username = ?1 AND u.password = ?2", nativeQuery = true)
    User getUserByUsernameAndPassword(String username, String password);

}
