package server.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import server.entity.Room;

@Repository
public interface RoomDAO extends JpaRepository<Room, Integer> {

}
