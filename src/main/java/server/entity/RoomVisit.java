package server.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class RoomVisit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "room_id")
    private Room roomId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User userId;

    @Column(name = "last_time_in_room")
    private Timestamp lastTimeInRoom;

    public RoomVisit() { }

    public RoomVisit(int id, Room roomId, User userId, Timestamp lastTimeInRoom) {
        this.id = id;
        this.roomId = roomId;
        this.userId = userId;
        this.lastTimeInRoom = lastTimeInRoom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Room getRoomId() {
        return roomId;
    }

    public void setRoomId(Room roomId) {
        this.roomId = roomId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Timestamp getLastTimeInRoom() {
        return lastTimeInRoom;
    }

    public void setLastTimeInRoom(Timestamp lastTimeInRoom) {
        this.lastTimeInRoom = lastTimeInRoom;
    }

    @Override
    public String toString() {
        return "RoomVisit{" +
                "id=" + id +
                ", roomId=" + roomId +
                ", userId=" + userId +
                ", lastTimeInRoom=" + lastTimeInRoom +
                '}';
    }

}
