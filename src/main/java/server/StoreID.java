package server;

public class StoreID {

    private static ThreadLocal<Integer>  threadLocal = new ThreadLocal<>();

    public static void setId(int id) {
        threadLocal.set(id);
    }

    public static Integer getId() {
        return threadLocal.get();
    }

    public static void remove() {
        threadLocal.remove();
    }

}
